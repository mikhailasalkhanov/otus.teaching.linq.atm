﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            
            System.Console.WriteLine("Выберите запрос:" +
                "\n1.Вывод информации о заданном аккаунте по логину и паролю" +
                "\n2.Вывод данных о всех счетах заданного пользователя" +
                "\n3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту" +
                "\n4.Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта" +
                "\n5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)");

            var code = System.Console.ReadLine();
            switch (code)
            {
                case "1":
                    var user = atmManager.GetUserByLoginAndPassword("cop", "555");
                    System.Console.WriteLine(user?.ToString() ?? "Неверный логин или пароль");
                    break;
                case "2":
                    var userAccounts = atmManager.GetUserAccounts(1);
                    Write<Account>(userAccounts);
                    break;
                case "3":
                    var userAccountsWithHistory = atmManager.GetUserAccountsWithHistory(2);
                    Write<AccountWithHistory>(userAccountsWithHistory);
                    break;
                case "4":
                    var inputCashOperations = atmManager.GetInputCashOperations();
                    Write<OperationByUser>(inputCashOperations);
                    break;
                case "5":
                    var usersTotalCash = atmManager.GetUsersTotalCashGreatherThan(100000);
                    Write<UserTotalCash>(usersTotalCash);
                    break;
                default:
                    break;
            }
            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

        static void Write<T>(IEnumerable<T> collection)
        {
            if (collection.Count() == 0)
            {
                System.Console.WriteLine("Ничего не найдено");
            }
            foreach (var item in collection)
            {
                System.Console.WriteLine(item.ToString());
            }            
        }
    }
}