﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class UserTotalCash
    {
        public User User { get; set; }
        public decimal TotalCash { get; set; }

        public override string ToString()
        {
            return $"{User.ToString()}\nБаланс: {TotalCash}\n";
        }
    }
}
