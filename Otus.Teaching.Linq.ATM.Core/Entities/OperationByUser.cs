﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class OperationByUser
    {
        public User User { get; set; }
        public OperationsHistory Operation { get; set; }

        public override string ToString()
        {
            return $"Операция:\n{Operation.ToString()}Пользователь:\n{User.ToString()}\n";
        }
    }
}
