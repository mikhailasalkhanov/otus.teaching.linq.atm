﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string PassportSeriesAndNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public bool IsAutentic(string login, string password) =>
            Login == login && Password == password;

        public override string ToString()
        {
            return $"Имя: {FirstName}\nОтчество: {MiddleName}\nФамилия: {SurName}\nТелефон: {Phone}" +
                $"\nПаспорт: {PassportSeriesAndNumber}\nДата регистрации: {RegistrationDate}";
        }
    }
}