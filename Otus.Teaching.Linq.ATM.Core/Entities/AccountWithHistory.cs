﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class AccountWithHistory
    {
        public Account Account { get; set; }
        public IEnumerable<OperationsHistory> History { get; set; }

        public override string ToString()
        {
            var history = new StringBuilder();
            foreach (var operation in History)
            {
                history.Append(operation.ToString());
            }
            return $"Счет:\n{Account.ToString()}\n История:\n {history}";
        }
    }
}
