﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        /// <summary>
        /// Вывод информации о заданном аккаунте по логину и паролю
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User GetUserByLoginAndPassword(string login, string password) =>
            Users.FirstOrDefault(u => u.IsAutentic(login, password));

        /// <summary>
        /// Вывод данных о всех счетах заданного пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Account> GetUserAccounts(int userId) =>
            Accounts.Where(acc => acc.UserId == userId);

        /// <summary>
        /// Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<AccountWithHistory> GetUserAccountsWithHistory(int userId)
        {
            return Accounts.Where(acc => acc.UserId == userId)
                .GroupJoin(History, acc => acc.Id, operation => operation.AccountId,
                (account, accountHistory) => new AccountWithHistory()
                {
                    Account = account,
                    History = accountHistory
                });
        }

        /// <summary>
        /// Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта
        /// </summary>
        /// <returns></returns>
        public IEnumerable<OperationByUser> GetInputCashOperations()
        {
            var inputOperations = History.Where(operation => operation.OperationType == OperationType.InputCash);

            var inputOperationsWithAccounts = inputOperations
                .Join(Accounts, operation => operation.AccountId, account => account.Id,
                    (operation, account) => new { operation, account });

            var inputOperationsByUsers = inputOperationsWithAccounts
                .Join(Users, operation => operation.account.UserId, user => user.Id, 
                    (operationWithAccount, user) =>
                        new OperationByUser() { Operation = operationWithAccount.operation, User = user});
            return inputOperationsByUsers;
        }

        /// <summary>
        /// Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public IEnumerable<UserTotalCash> GetUsersTotalCashGreatherThan(decimal amount)
        {
            return Users.GroupJoin(Accounts, user => user.Id, account => account.UserId,
                (user, accounts) => new UserTotalCash()
                {
                    User = user,
                    TotalCash = accounts.Sum(account => account.CashAll)
                })
                .Where(user => user.TotalCash > amount);
        }
    }
}